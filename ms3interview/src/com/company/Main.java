package com.company;

import java.io.FileReader;
import java.io.FileWriter;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import com.opencsv.*;
import com.opencsv.enums.CSVReaderNullFieldIndicator;


public class Main {

    public static void main(String[] args) throws Exception {

        CSVParser parser = new CSVParserBuilder().withSeparator(',').withQuoteChar('"').withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS).build();
        CSVReader reader = new CSVReaderBuilder(new FileReader("ms3Interview.csv")).withCSVParser(parser).withSkipLines(1).build();
        CSVReader readHeader = new CSVReaderBuilder(new FileReader("ms3Interview.csv")).withCSVParser(parser).withSkipLines(0).build();

        boolean isNull = false;
        int received = 0;
        int successful = 0;
        int failed = 0;

        String url = "jdbc:mysql://localhost:3306/?allowMultiQueries=true";
        String username = "java";
        String password = "password";

        StringBuilder csv = new StringBuilder("");
        StringBuilder logfile = new StringBuilder("");

        String[] nextLine;
        String[] header;
        List<String[]> goodLine = new ArrayList<>();
        List<String[]> badLine = new ArrayList<>();


        //iterating through each line
        while ((nextLine = reader.readNext()) != null) {

            isNull = false;

            //it's okay, I'll just wait here, I'm sure you'll uncomment me eventually :(

            /*for (int j = 0; j < (nextLine.length -1); j++) {

                if(nextLine[j] != null) {
                    if(j < (nextLine.length - 1)) {
                        wholeLine.append(nextLine[j] + ", ");
                    } else {
                        wholeLine.append(nextLine[j]);
                    }
                } else {
                    wholeLine.append(",");
                    isNull = true;
                }
            }*/

            //do you feel sorry for that loop? That is because you crazy. It has no feelings. Besides, the new one is much better!
            for (String line : nextLine) {
                if(line == null) {
                    isNull = true;
                }
            }

            //adding to bad or good lines
            if(isNull) {
                badLine.add(nextLine);
            } else {
                goodLine.add(nextLine);
            }

        }

        //connecting to database
        System.out.println("Connecting to database...");

        Class.forName("com.mysql.jdbc.Driver");

        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            System.out.println("Connection successful");

            //creating table
            Statement statement;
            statement = connection.createStatement();
            String createDB = "CREATE DATABASE IF NOT EXISTS MS3;";
            String createTable = "USE MS3;\n" +
                    " DROP TABLE IF EXISTS CSV;\n" +
                    " CREATE TABLE CSV (a VARCHAR(50), b VARCHAR(50), c VARCHAR(50), d VARCHAR(50), e VARCHAR(2000), f VARCHAR(50), g VARCHAR(50), h VARCHAR(50), i VARCHAR(50), j VARCHAR(50));";

            statement.executeUpdate(createDB);
            statement.executeUpdate(createTable);
            System.out.println("Table created");

            //inserting data
            System.out.println("Starting to insert data...");
            String query = "INSERT INTO CSV VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            connection.setAutoCommit(false);
            PreparedStatement prep = connection.prepareStatement(query);
            for (String[] gline : goodLine) {
                prep.setString(1, gline[0]);
                prep.setString(2, gline[1]);
                prep.setString(3, gline[2]);
                prep.setString(4, gline[3]);
                prep.setString(5, gline[4]);
                prep.setString(6, gline[5]);
                prep.setString(7, gline[6]);
                prep.setString(8, gline[7]);
                prep.setString(9, gline[8]);
                prep.setString(10, gline[9]);
                prep.addBatch();

            }
            prep.executeBatch();
            connection.commit();
            System.out.println("Finished inserting data");

            connection.close();
            System.out.println("Connection closed");

        } catch(SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("Connection failed");
        }



        //finding successful/failed/total
        successful = goodLine.size();
        failed = badLine.size();
        received = successful + failed;

        //getting timestamp
        Date date = new Date();
        SimpleDateFormat fdate = new SimpleDateFormat("HH-mm-ss");

        //making log file
        logfile.append("csv-" + fdate.format(date) + ".log");
        FileHandler handler = new FileHandler(logfile.toString());
        Logger logger = Logger.getLogger("string");
        logger.addHandler(handler);
        logger.info("Received: " + received);
        logger.info("Successful: " + successful);
        logger.warning("Failed: " + failed);

        //checking first line of each bad/good lines
        System.out.println("First Success = " + Arrays.toString(goodLine.get(0)));
        System.out.println("First Failure = " + Arrays.toString(badLine.get(0)));

        //putting bad data into csv file
        csv.append("bad-data-" + fdate.format(date) + ".csv");
        FileWriter file = new FileWriter(csv.toString(), true);
        CSVWriter writer = new CSVWriter(file);

        if((header = readHeader.readNext()) != null) {
            writer.writeNext(header);
        }
        writer.writeAll(badLine, true);
    }
}
