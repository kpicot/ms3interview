# README #



### What is this repository for? ###

Java programming challenge for MS3

### How do I get set up? ###

It uses the following libraries:

* MySQL Connector/J 5.1.6
* OpenCSV 3.9 with the following dependencies
- commons-beanutils 1.9.3
- org.apache.commons 4.1
- commons-collections 3.5
- commons-logging 1.2

These should be included in the classpath for the Intellij/Eclipse projects in the repository. It will connect to a local MySQL connection with "java/password"

It needs ms3Interview.csv in the same folder

### Who do I talk to? ###

Kevin Picot